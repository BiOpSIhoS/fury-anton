#Hello! This is Fury!

#### Project map

- index.html
- modules.html
- README.md  
- css/  
  -- normalize.css
  -- style.css  
- js/  
  -- jquery-min.js
  -- html5shiv.js  
- images/  
- resources/  
  - demo/    
    - css/
    - index-(1-10).html    
  - html/    
    - proto-(1-10).html      
    - snippetsST/      
      - proto-(1-10).sublime-snippet   
  - **scss**/    
    - style.scss
    - _variables.scss
    - _mixins.scss
    - _grids.scss
    - _base.scss
    - _form.scss
    - _navi.scss
    - _pre-news.scss
    - _catalog.scss
    - _разныемодули.scss
    

**Если не получилось дерево в project map, то это как всегда битбакет не в своем дворе выебуется**